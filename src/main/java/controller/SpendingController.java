package controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.backend.GiftcardGetResponse;
import data.model.giftcards.Giftcard;
import data.model.giftcards.util.GiftcardUtil;
import repo.GiftcardRepo;
import rest.service.error.ServiceError;
import util.general.VersionController;

@Service
public class SpendingController {
	
	@Autowired
	private GiftcardRepo giftcardRepo;

	public GiftcardGetResponse getCardsToSpend(double amount) throws ServiceError{
		List<Giftcard> cards = giftcardRepo.findByCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFIED);
		cards = this.sortCards(cards);
		cards = this.reduceList(cards, amount);
		
		if(cards.size() == 0)
			throw new ServiceError("No cards available"); 
		
		for(Giftcard card : cards) { 
			card.setCurrentStatus(VersionController.GIFTCARD_STATUS_SPENDING);
			card.setLastUpdate(LocalDate.now());
			giftcardRepo.save(card);
		}
		
		GiftcardGetResponse response = new GiftcardGetResponse();
		response.setGiftcards(GiftcardUtil.convertToBackendList(cards));
		return response;
	}
	
	public String cardSpent(String cardNumber) throws ServiceError{
		Giftcard card = giftcardRepo.findByNumber(cardNumber).get(0);
		if(card == null)
			throw new ServiceError("Invalid card number");
		
		card.setCurrentStatus(VersionController.GIFTCARD_STATUS_PAYING);
		card.setLastUpdate(LocalDate.now());
		giftcardRepo.save(card);
		return "Good";
	}
	
	public String spendingIssue(String cardNumber, String spendingIssueReason, double totalSpent) throws ServiceError{
		Giftcard card = giftcardRepo.findByNumber(cardNumber).get(0);
		if(card == null)
			throw new ServiceError("Invalid card number");
		
		card.setCurrentStatus(VersionController.GIFTCARD_STATUS_SPENDING_PROMBLEM);
		card.setFailedSpendingReason(spendingIssueReason);
		card.setTotalSpent(totalSpent);
		card.setLastUpdate(LocalDate.now());
	
		giftcardRepo.save(card);
		return "Good";
	}
	
	private List<Giftcard> reduceList(List<Giftcard> cards, double amount) {
		double currentAmount = 0;
		List<Giftcard> reduced = new ArrayList<>();
		for(Giftcard card : cards) {
			reduced.add(card);
			currentAmount+=card.getVerifiedBalance();
			if(currentAmount > amount)
				break;
		}
		return reduced;
	}
	
	private List<Giftcard> sortCards(List<Giftcard> cards) {
		for(int i = 0; i < cards.size()-1; i++) 
			for(int ii = 0; ii < cards.size()-1-i; ii++) 
				if(cards.get(ii).getVerifiedDate().isAfter(cards.get(ii+1).getVerifiedDate())) {
					Giftcard temp = cards.get(ii);
					cards.set(ii, cards.get(ii+1));
					cards.set(ii+1, temp);
				}
		return cards;
	}

}
