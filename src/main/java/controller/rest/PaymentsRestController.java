package controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import controller.PaymentsController;
import data.model.payments.Payment;
import data.model.payments.rest.PaymentSubmit;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@RequestMapping("/api/payments/")
public class PaymentsRestController {
	
	@Autowired
	private PaymentsController controller;
	
	@GetMapping("needspayment")
	public ResponseEntity<Object> getAllNeedsPayment() {
		List<Payment> payments = controller.needsPayment();
		return ResponseEntity.ok(payments);		
	}
	
	@PostMapping("payment/sent")
	public ResponseEntity<Object> paymentSubmitted(@RequestBody PaymentSubmit paymentSubmit) {
		try {
			String response = controller.paymentSubmit(paymentSubmit);
			return ResponseEntity.ok(new BasicResponse(response));
		} catch(ServiceError error) {
			return ResponseEntity.badRequest().body(error.getResponseError());
		}
	}
	
}
