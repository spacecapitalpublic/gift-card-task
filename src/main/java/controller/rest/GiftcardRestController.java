package controller.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import controller.SpendingController;
import data.model.backend.GiftcardGetResponse;
import data.model.backend.GiftcardSpendRequest;
import data.model.backend.GiftcardSpendingIssueRequest;
import rest.service.error.ServiceError;
import util.response.BasicResponse;

@RestController
@RequestMapping("/api/giftcards/")
public class GiftcardRestController {
	
	@Autowired
	private SpendingController spendingController;
	
	@GetMapping("getCards")
	public ResponseEntity<Object> getCardsToSpend() {
		try {
			GiftcardGetResponse response = spendingController.getCardsToSpend(5000);
			return ResponseEntity.ok(response);
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("cardspent/{cardNumber}")
	public ResponseEntity<Object> cardSpent(@PathVariable String cardNumber) {
		try {
			String response = this.spendingController.cardSpent(cardNumber);
			return ResponseEntity.ok(new BasicResponse(response)); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
	@PostMapping("spendingIssue/{cardNumber}")
	public ResponseEntity<Object> cardSpendingIssue(@PathVariable String cardNumber, @RequestBody GiftcardSpendingIssueRequest request) {
		try {
			String response = this.spendingController.spendingIssue(cardNumber, request.getReason(), request.getTotalSpent());
			return ResponseEntity.ok(new BasicResponse(response)); 
		} catch (ServiceError e) {
			return ResponseEntity.badRequest().body(new BasicResponse(e.getResponseError()));
		}
	}
	
}
