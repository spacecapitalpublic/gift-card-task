package controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.accounts.AccountPayements;
import data.model.giftcards.Giftcard;
import data.model.payment.dates.PaymentDate;
import data.model.payments.Payment;
import data.model.payments.PaymentLine;
import repo.AccountPayementRepo;
import repo.GiftcardRepo;
import repo.PaymentDateRepo;
import repo.PaymentsRepo;
import util.general.VersionController;

@Service
public class PayoutController {

	@Autowired
	private GiftcardRepo giftcardRepo;
	
	@Autowired
	private AccountPayementRepo accountPaymentsRepo;
	
	@Autowired
	private PaymentsRepo paymentRepo;
	
	@Autowired
	private PaymentDateRepo paymentDateRepo;
	
	
	public void run() {
		updatePaymentDates(); //check payment dates and determine if they are still active
		assignPayments(); //giftcards get assigned to a payment
		setPaymentMethods(); //set payment methods not active if the payment date is no longer active and if the payment method is not NONE.
	}
	
	private void setPaymentMethods() {
		List<Payment> payments = paymentRepo.findByStatus(VersionController.PAYMENT_STATUS_ACTIVE);
		
		for(Payment pay : payments) {
			AccountPayements account = accountPaymentsRepo.findByEmail(pay.getEmail());
			pay.setMethod(account.getPreferredPayementMethod());
			
			if(pay.getMethod().equals(VersionController.PAYMENT_NONE)) {
				pay.setAccountNumber("");
				pay.setRouting("");
				pay.setPaypalEmail("");
			} else if(pay.getMethod().equals(VersionController.PAYMENT_PAYPAL)) {
				pay.setPaypalEmail(account.getPaypalEmail());
				pay.setAccountNumber("");
				pay.setRouting("");
			} else if(pay.getMethod().equals(VersionController.PAYMENT_ECHECK)) {
				pay.setPaypalEmail("");
				pay.setAccountNumber(account.getAccountNumber());
				pay.setRouting(account.getRoutingNumber());
			}
			
			if(!pay.getMethod().equals(VersionController.PAYMENT_NONE)) {
				PaymentDate paymentDate = paymentDateRepo.findByPaymentId(pay.getPaymentDateId());
				if(!paymentDate.isActive())
					pay.setStatus(VersionController.PAYMENT_STATUS_PAYING);
			}
			paymentRepo.save(pay);
		}
	}
	
	//send gift cards to their payments 
	private void assignPayments() {
		this.updatePaymentDates();
		PaymentDate date = this.getCurrentPaymentDate();
		List<Giftcard> cards = giftcardRepo.findByCurrentStatus(VersionController.GIFTCARD_STATUS_PAYING);
		
		for(Giftcard card : cards) 
			this.scheduleCard(card, date);
	}
	
	private void scheduleCard(Giftcard card, PaymentDate currentDate) {
		String email = card.getEmail();
		List<Payment> payments = paymentRepo.findByEmailAndStatus(email, VersionController.PAYMENT_STATUS_ACTIVE);
		Payment payment = null;
		
		if(payments.size() == 1) 
			payment = payments.get(0);

		if(payment == null) {
			payment = new Payment();
			payment.setLines(new HashMap<String, PaymentLine>());
			payment.setCardNumbers(new HashSet<String>());
			payment.setPaid(false);
			payment.setPaymentDate(currentDate.getDate());
			payment.setPaymentDateId(currentDate.getPaymentId());
			payment.setPaymentId("Payment-"+paymentRepo.findByEmail(email).size());
			payment.setStatus(VersionController.PAYMENT_STATUS_ACTIVE);
			payment.setEmail(card.getEmail());
		}

		payment.getCardNumbers().add(card.getNumber());
		
		PaymentLine line = new PaymentLine();
		line.setCardNumber(card.getNumber());
		line.setPayoutBalance(card.getPayoutBalance());
		line.setBalance(card.getVerifiedBalance());
		line.setPayoutRate(card.getPayoutRate());
		
		payment.getLines().put(line.getCardNumber(), line); 
		
		double payoutBalance = 0;
		for(String s : payment.getLines().keySet()) {
			payoutBalance+=payment.getLines().get(s).getPayoutBalance();
		}
		
		payment.setTotalPayout(payoutBalance);
		
		
		paymentRepo.save(payment); 
		
		card.setLastUpdate(LocalDate.now());
		card.setCurrentStatus(VersionController.GIFTCARD_STATUS_PAYING_SCHEDULED);
		card.setPaymentDate(currentDate.getDate());
		card.setPaymentDateId(currentDate.getPaymentId());
		giftcardRepo.save(card); 
	}
	
	private void updatePaymentDates() {
		List<PaymentDate> dates = paymentDateRepo.findByActive(true); 
		LocalDate now = LocalDate.now().plusDays(2);
		for(PaymentDate date : dates) 
			if(date.getDate().isBefore(now)) {
				date.setActive(false);
				paymentDateRepo.save(date);
			}
		
//		List<Payment> payments = paymentRepo.findByStatus(VersionController.PAYMENT_STATUS_ACTIVE);
//		for(Payment payment : payments) {
//			if(payment.getMethod().equals(VersionController.PAYMENT_NONE)) 
//				continue;
//			
//			if(payment.getPaymentDate().isBefore(now)) {
//				payment.setStatus(VersionController.PAYMENT_STATUS_PAYING);
//				paymentRepo.save(payment);
//			}
//		}
		
	}
	
	private PaymentDate getCurrentPaymentDate() {
		List<PaymentDate> dates = paymentDateRepo.findByActive(true); 
		PaymentDate currentDate = null;
		
		LocalDate now = LocalDate.now().plusDays(1);
		for(PaymentDate date : dates) 
			if(date.getDate().isAfter(now)) {
				if(currentDate == null)
					currentDate = date;
				else {
					if(currentDate.getDate().isAfter(date.getDate()))
						currentDate = date;
				}
			}
		return currentDate;
	}
	
}
