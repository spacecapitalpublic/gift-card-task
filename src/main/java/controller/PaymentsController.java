package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.payments.Payment;
import data.model.payments.rest.PaymentSubmit;
import repo.PaymentsRepo;
import rest.service.error.ServiceError;
import util.general.VersionController;

@Service
public class PaymentsController {

	@Autowired
	private PaymentsRepo paymentsRepo;	
	
	public List<Payment> needsPayment() {
		return paymentsRepo.findByStatus(VersionController.PAYMENT_STATUS_PAYING);
	}
	
	public String paymentSubmit(PaymentSubmit paymentSubmit) throws ServiceError {
		Payment payment = paymentsRepo.findByEmailAndPaymentId(paymentSubmit.getEmail(), paymentSubmit.getPaymentId());
		if(payment == null)
			throw new ServiceError("Invlaid email and id combo");
		if(!payment.getId().equals(paymentSubmit.getDatabaseId()))
			throw new ServiceError("Wrong database id vs email/id combo");
		
		payment.setStatus(VersionController.PAYMENT_STATUS_PAID);
		payment.setAchTransId(paymentSubmit.getAchTransId());
		payment.setPaypalTransId(paymentSubmit.getPaypalTransId());
		
		paymentsRepo.save(payment);
		return "Payment updated";
	}
}
