package controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import data.model.giftcards.Giftcard;
import homedepot.GiftcardChecker;
import repo.GiftcardRepo;
import util.general.VersionController;

@Service
public class VerifyController {
	
	@Autowired
	private GiftcardRepo giftcardRepo;

	public void run() {
		System.out.println("Running the verify controller");
		List<Giftcard> cards = giftcardRepo.findByCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING);
		HashMap<String, Double> balances = this.getBalances(cards); 
		this.verifyCards(cards, balances);
		System.out.println("Finished running the verify controller");
	}
	
	private void verifyCards(List<Giftcard> cards, HashMap<String, Double> balances) {
		for(Giftcard card : cards) {
			double proclaimedBalance = card.getProclaimedBalance();
			double verifyBalance = balances.get(card.getNumber());
			
			//verify that no fuckery is going on
			List<Giftcard> databaseCards = giftcardRepo.findByNumber(card.getNumber());
			if(databaseCards.size() > 1) {
				card.setLastUpdate(LocalDate.now());
				card.setFailedVerifyReason("Duplicate card number detected");
				card.setErrorType(VersionController.GIFTCARD_ERROR_VERIFY_DUPLICATE);
				card.setVerifiedDate(LocalDate.now());
				card.setLastUpdate(LocalDate.now());
				card.setPayoutBalance(0);
				card.setCanReverify(false);
				
			} else {
				if(verifyBalance == -1) {
					//do nothing, just retry again 
					card.setLastUpdate(LocalDate.now());
				} else if(verifyBalance == -99) {
					card.setVerifiedBalance(0);
					card.setFailedVerifyReason("Card data is invalid (either card number or pin) or is a in-store only credit");
					card.setErrorType(VersionController.GIFTCARD_ERROR_VERIFY_DETAILS);
					card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM);
					card.setVerifiedDate(LocalDate.now());
					card.setLastUpdate(LocalDate.now());
					card.setPayoutBalance(0);
					card.setCanReverify(false);
				} else if(verifyBalance == -98) {
					card.setVerifiedBalance(0);
					card.setFailedVerifyReason("Card data is invalid (either card number or pin) or is a in-store only credit");
					card.setErrorType(VersionController.GIFTCARD_ERROR_VERIFY_DETAILS);
					card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM);
					card.setVerifiedDate(LocalDate.now());
					card.setLastUpdate(LocalDate.now());
					card.setPayoutBalance(0);
					card.setCanReverify(false);
				} else {
					double diff = Math.abs(proclaimedBalance-verifyBalance);
					if(diff < .25) {
						card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFIED);
						card.setVerifiedBalance(verifyBalance);
						card.setPayoutBalance(proclaimedBalance*card.getPayoutRate());
						card.setVerifiedDate(LocalDate.now());
						card.setLastUpdate(LocalDate.now());
					} else {
						card.setVerifiedBalance(verifyBalance);
						card.setFailedVerifyReason("Proclaimed balance is incorrect " + proclaimedBalance + " actual balance: " + verifyBalance);
						card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING_PROMBLEM);
						card.setErrorType(VersionController.GIFTCARD_ERROR_VERIFY_BALANCE);
						card.setVerifiedDate(LocalDate.now());
						card.setLastUpdate(LocalDate.now());
						card.setPayoutBalance(0);
						card.setCanReverify(true);
					}
				}
			}
			giftcardRepo.save(card); 
		}
	}
	
	private HashMap<String, Double> getBalances(List<Giftcard> cards) {
		GiftcardChecker checker = new GiftcardChecker(cards, 10);
		checker.run();
		return checker.getBalanceMap();
	}
	
}
