package homedepot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import data.model.giftcards.Giftcard;
import util.general.GeneralUtil;

public class GiftcardChecker {
	
	private List<Giftcard> cards;
	private int threadCount;
	
	private HashMap<String, Double> balanceMap;
	
	public GiftcardChecker(List<Giftcard> cards, int threadCount) {
		this.cards = cards;
		this.threadCount = threadCount;
	}
	
	public void run() {
		List<List<Giftcard>> split = GeneralUtil.splitArrayList(cards, threadCount);
		
		List<GiftcardCheckerThread> threads = new ArrayList<>();
		for(List<Giftcard> list : split) {
			threads.add(new GiftcardCheckerThread(list));
			threads.get(threads.size()-1).start();
		}
		
		boolean running = true;
		
		while(running) {
			running = false;
			int progress = 0;
			int threadCount = 0;
			for(GiftcardCheckerThread thread : threads) {
				if(thread.isRunning()) {
					threadCount++;
					running = true;
				}
				progress+=thread.getCurrentCount();
			}
			GeneralUtil.print("Progress count: " + progress + "("+threadCount+")");
			this.sleep(60000);
		}
		
		balanceMap = new HashMap<String, Double>();
		for(GiftcardCheckerThread thread : threads) {
			for(String s : thread.getCardBalances().keySet()) {
				balanceMap.put(s, thread.getCardBalances().get(s));
			}
		}
	}
	
	public HashMap<String, Double> getBalanceMap() {
		return this.balanceMap;}
	
	
	private void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	

}
