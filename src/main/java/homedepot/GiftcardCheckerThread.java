package homedepot;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import data.model.giftcards.Giftcard;
import util.http.ProxyUtil;
import util.recaptcha.RecaptchaSolver;

public class GiftcardCheckerThread extends Thread{
	
	private List<Giftcard> cards;
	private HashMap<String, Double> cardBalances;
	private int currentCount;
	private boolean running;
	
	public GiftcardCheckerThread(List<Giftcard> cards) {
		this.cards = cards;
		this.cardBalances = new HashMap<String, Double>();
		this.currentCount = 0;
		this.running = true;
	}

	public void run() {
		for(Giftcard card : cards) {
			for(int i = 0; i < 5; i++) {
				boolean success = this.runCard(card);
				if(success)
					break;
			}
			
			if(cardBalances.get(card.getNumber()) != null && cardBalances.get(card.getNumber()) != card.getProclaimedBalance() && cardBalances.get(card.getNumber()) != -1) {
//				GeneralUtil.print(card.getNumber() + " " + card.getPin() + " " + card.getProclaimedBalance() + " | " + cardBalances.get(card.getNumber()));
			}
			currentCount++;
		}
		running = false;
	}
	
	private boolean runCard(Giftcard card) {
		String solvedCode = this.getSolved();
		if(solvedCode.equals(""))
			return false; 
		HttpPost post = this.createPost(card.getNumber(), card.getPin(), solvedCode);
		ArrayList<Object> response = ProxyUtil.executeRequest(post, true);
		double balance = this.getBalance((String) response.get(1)); 
		cardBalances.put(card.getNumber(), balance); 
		return balance != -1;
	}
	
	private double getBalance(String body) {
		try {
			JsonNode node = new ObjectMapper().readTree(body);
			
			if(node.get("giftCards").get("giftCard").get("errorCode") != null) {
				String errorDesc = node.get("giftCards").get("giftCard").get("description").asText();
				if(errorDesc.contains("ask a cashier to check the balance for you"))
					return -99;
				if(errorDesc.contains("We are sorry, there is a slight problem with your Gift"))
					return -98;
			}
			double value = node.get("giftCards").get("giftCard").get("availableAmount").asDouble();
			return value;
		} catch (JsonProcessingException e) {
		} catch (IOException e) {
		} catch(NullPointerException e) {
//			GeneralUtil.print(body);
		}
		return -1;
	}
	
	private HttpPost createPost(String cardNumber, String cardPin, String solvedCode) {
		HttpPost post = new HttpPost("https://secure2.homedepot.com/mcc-checkout/v2/giftcard/balancecheck");
		
		try {
			post.setEntity(new StringEntity(createPayload(cardNumber, cardPin, solvedCode)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		post.setHeader("Content-Type","application/json");
		post.setHeader("Accept","application/json");
		
		return post;
	}
	
	private String createPayload(String cardNumber, String cardPin, String solved) {
		String payload = "{\"GiftCardsRequest\":{\"cardNumber\":\""+cardNumber+"\",\"pinNumber\":\""+cardPin+"\",\"reCaptcha\":\""+solved+"\"}}";
		return payload;
	}
	
	private String getSolved() {
		String solved = RecaptchaSolver.solveRecaptcha("6LfEHBkTAAAAAHX6YgeUw9x1Sutr7EzhMdpbIfWJ", "https://secure2.homedepot.com/mycheckout/giftcard#/giftcard");
		return solved;
	}
	
	public HashMap<String, Double> getCardBalances() {return cardBalances;}
	public int getCurrentCount() {return currentCount;}
	public boolean isRunning() {return running;}
}
