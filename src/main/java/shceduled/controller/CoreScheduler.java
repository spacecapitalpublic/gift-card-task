package shceduled.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import controller.PayoutController;
import controller.VerifyController;
import data.model.offers.Offer;
import repo.OffersRepo;
import repo.PaymentsRepo;
import util.general.VersionController;

@Service
public class CoreScheduler {
	
	@Autowired
	private PaymentsRepo repo;
	
	@Autowired
	private VerifyController verifyController;
	
	@Autowired
	private PayoutController payoutController;
		
	
	@Scheduled(fixedDelay=(1000*60*60))
	public void run() {
		verifyController.run();
		payoutController.run();
		
	}
	
	public void productionRun() {
		//verify cards
		verifyController.run();
		
		//payments
		payoutController.run();
		
		
	}
	
	

}
