package util.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import javax.net.ssl.SSLHandshakeException;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProxyUtil {
	
	private static ObjectMapper objectMapper = new ObjectMapper();;
	private final static HttpHost proxy = new HttpHost("5.79.66.2", 13200);
	private final static DefaultProxyRoutePlanner planner = new DefaultProxyRoutePlanner(proxy); 
	
	
	private final static CloseableHttpClient quickClient = 
			HttpClientBuilder.create().disableCookieManagement().disableAuthCaching()
			.disableRedirectHandling().setKeepAliveStrategy(DefaultConnectionKeepAliveStrategy.INSTANCE).build();
	
	
	private final static CloseableHttpClient proxyClient = 
			HttpClientBuilder.create().disableCookieManagement().disableAuthCaching()
			.disableRedirectHandling().setRoutePlanner(planner).build();

	public static void start() {
		objectMapper = new ObjectMapper();
	}
	
	public static JsonNode exectuteHttpRequest(HttpRequestBase httpRequest, boolean useProxy, long timeout) {
		ArrayList<Object> items = executeRequest(httpRequest, useProxy);
			
		
		CloseableHttpResponse response = (CloseableHttpResponse)items.get(0);

		StatusLine line = response.getStatusLine();
		if(line.getStatusCode() != 200) 
			if((line.getStatusCode() == 502 || line.getStatusCode() == 403) && useProxy) 
				return exectuteHttpRequest(httpRequest, useProxy, timeout); 
		
		JsonNode node = getReponseJson((String)items.get(1));
		httpRequest.releaseConnection();
		try {
			response.close();
		} catch (IOException e) {
		}
		return node;
	}
	
	public static HttpResponse getResponse(HttpRequestBase httpRequest, boolean useProxy, long timeout) {
		HttpResponse response = (CloseableHttpResponse)executeRequest(httpRequest, useProxy).get(0); 
		return response;
	}
	
	
	public static Document getJsoupDoc(HttpRequestBase request, boolean useProxy, long timeout, int trys) {
		ArrayList<Object> list = executeRequest(request, useProxy);
		
		
		HttpResponse response = (CloseableHttpResponse)list.get(0); 
		StatusLine line = response.getStatusLine();
		if(line.getStatusCode() != 200) 
			if((line.getStatusCode() == 502 || line.getStatusCode() == 403) && useProxy) {
				request.releaseConnection();
				return getJsoupDoc(request, useProxy, timeout, trys); 
			}
		
		Document doc = getJsoupDocFromResponse((String)list.get(1)); 
		request.releaseConnection();
		return doc;
	}
	
	public static ArrayList<Object> getJsoupDocWithHeaders(HttpRequestBase request, boolean useProxy, long timeout, int trys) {
		ArrayList<Object> list = executeRequest(request, useProxy);

		HttpResponse response = (CloseableHttpResponse)list.get(0);
		StatusLine line = response.getStatusLine();
		if(line.getStatusCode() != 200) 
			if((line.getStatusCode() == 502 || line.getStatusCode() == 403) && useProxy) 
				return getJsoupDocWithHeaders(request, useProxy, timeout, trys); 
		
		Document doc = getJsoupDocFromResponse((String)list.get(1));
		Header[] headers = response.getAllHeaders();
		ArrayList<Object> data = new ArrayList<>();
		data.add(headers);
		data.add(doc);
		return data;
	}
	
	public static Document getJsoupDocFromResponse(String response) {
		try {
	        Document doc = Jsoup.parse(response);
	        return doc;
		} catch (UnsupportedOperationException e) {}
		return null;
	}
	
	public static JsonNode getReponseJson(String message) {
		InputStream in;
		try {
			JsonNode node = objectMapper.readTree(message);
		    return node;
		} catch (UnsupportedOperationException e) {} catch (IOException e) {}
		return null;
	}
	
	
	
	public static ArrayList<Object> executeRequest(HttpRequestBase request, boolean proxy) {
		
		String body = "";
		CloseableHttpResponse response = null;
		
		CloseableHttpClient client = quickClient;
		if(proxy) client = proxyClient;
		
		try {
			response = client.execute(request, HttpClientContext.create());
			InputStream stream = response.getEntity().getContent(); 
			StringWriter writer = new StringWriter();
			IOUtils.copy(stream, writer, "UTF-8");
			body = writer.toString();
		} catch (ClientProtocolException e) {
			return executeRequest(request, proxy); 
		} catch (SSLHandshakeException e) {
			return executeRequest(request, proxy); 
		}catch(SocketTimeoutException e) {
			return executeRequest(request, proxy); 
		} catch(IOException e) {
			return executeRequest(request, proxy); 
		} finally {
			try {
				request.releaseConnection();
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch(NullPointerException e) {
				
			}
		}
		
		StatusLine statusLine = response.getStatusLine();
		if(statusLine.getStatusCode() != 200) 
			if(statusLine.getStatusCode() == 302 || statusLine.getStatusCode() == 301 || statusLine.getStatusCode() == 502) 
				for(Header header : response.getAllHeaders()) 
					if(header.getName().toLowerCase().contains("location"))  {
						request.releaseConnection();
						return executeRequest(new HttpGet(header.getValue()), proxy);
					}
		
		ArrayList<Object> list = new ArrayList<>();
		list.add(response);
		list.add(body);
		
		return list;
	}
	
	
}
