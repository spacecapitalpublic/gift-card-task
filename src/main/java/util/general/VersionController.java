package util.general;

public class VersionController {
	public static final String PAYMENT_NONE = "NONE";
	public static final String PAYMENT_PAYPAL = "PAYPAL";
	public static final String PAYMENT_ECHECK = "ECHECK";
	
	public static final String PAYMENT_STATUS_ACTIVE = "ACTIVE";
	public static final String PAYMENT_STATUS_PAYING = "PAYING";
	public static final String PAYMENT_STATUS_FAILED = "PAYMENT FAILED"; 
	public static final String PAYMENT_STATUS_PAID = "PAID";
	
	public static final String GIFTCARD_STATUS_VERIFYING = "VERIFYING";
	public static final String GIFTCARD_STATUS_VERIFYING_PROMBLEM = "VERIFYING PROBLEM";
	public static final String GIFTCARD_STATUS_VERIFIED = "VERIFIED";
	public static final String GIFTCARD_STATUS_SPENDING = "SPENDING";
	public static final String GIFTCARD_STATUS_SPENDING_PROMBLEM = "SPENDING PROBLEM";
	public static final String GIFTCARD_STATUS_PAYING = "SCHEDULING PAYMENT";
	public static final String GIFTCARD_STATUS_PAYING_SCHEDULED = "PAYMENT SCHEDULED";
	public static final String GIFTCARD_STATUS_PAID = "PAID";
	public static final String GIFTCARD_STATUS_SETTLED = "SETTLED"; //move it to a different collection 

	public static final String GIFTCARD_SEARCH_VERIFYING = "VERIFY";
	public static final String GIFTCARD_SEARCH_VERIFIED = "VERIFIED";
	public static final String GIFTCARD_SEARCH_PROBLEMS = "PROBLEMS";
	public static final String GIFTCARD_SEARCH_SPENDING = "SPENDING";
	public static final String GIFTCARD_SEARCH_PAYING = "PAYING";
	public static final String GIFTCARD_SEARCH_PAID = "PAID";
	public static final String GIFTCARD_SEARCH_SETTLED = "SETTLED"; 
	
	public static final String GIFTCARD_ERROR_VERIFY_BALANCE = "VERIFY BALANCE";
	public static final String GIFTCARD_ERROR_VERIFY_DETAILS = "VERIFY DETAILS"; 
	public static final String GIFTCARD_ERROR_VERIFY_OTHER = "VERIFY OTHER";
	public static final String GIFTCARD_ERROR_VERIFY_DUPLICATE = "VERIFY DUPLICATE";
	
	public static final String SOURCE_HOMEDEPOT = "HOME DEPOT"; 
	
}
