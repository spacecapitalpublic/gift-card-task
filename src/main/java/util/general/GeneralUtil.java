package util.general;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class GeneralUtil {
	
	private static final Random random = new Random();
	
	public static boolean isEmailValid(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ 
                "[a-zA-Z0-9_+&*-]+)*@" + 
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" + 
                "A-Z]{2,7}$"; 
                  
		Pattern pat = Pattern.compile(emailRegex); 
		if (email == null) 
		return false; 
		return pat.matcher(email).matches(); 
		
	}
	
	public static String generateRandomString(int length) {
		String set = "abcdefghijklmnopqrstuvxyz0123456789";
		String dope = "";
		for(int i = 0; i < length; i++) {
			dope+=set.charAt(random.nextInt(set.length()));
		}
		return dope;
	}
	
	public static String generateRandomStringNumbers(int length) {
		String set = "0123456789";
		String dope = "";
		for(int i = 0; i < length; i++) {
			dope+=set.charAt(random.nextInt(set.length()));
		}
		return dope;
	}
	
	public static <T>List<List<T>> splitArrayList(List<T> list, int parts) {
		List<List<T>> split = new ArrayList<>();
		int currentIndex = 0;
		for(T t : list) {
			if(split.size() <= currentIndex)
				split.add(new ArrayList<T>());
			split.get(currentIndex).add(t);
			currentIndex++;
			if(currentIndex >= parts) 
				currentIndex = 0;
		}
		return split;
	}
	
	public static synchronized void print(String s) {
		System.out.println(s);
	}	
	
//	public static String getTokenFromAuthHeader(String authHeader) {
//		if(!authHeader.startsWith("Bearer "))
//			return "";
//		if(authHeader.split("Bearer ").length < 1)
//			return "";
//		try {
//			return authHeader.split("Bearer ")[1];
//		} catch(Exception e) {
//			return "";
//		}
//	}
	
//	public static String getEmailFromTokenFromHeader(String authHeader) {
//		String token = getTokenFromAuthHeader(authHeader);
//		if(token.equals(""))
//			return "";
//		else
//			return TokenManager.findEmailByToken(token);
//	}
	

}
