package util.recaptcha;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import util.general.GeneralUtil;
import util.http.ProxyUtil;

public class RecaptchaSolver {
	
	private static final String apiKey = "RedactedForPublic";
	private static final String startingEndpoint = "https://2captcha.com/in.php?key=%s&method=%s&googlekey=%s&pageurl=%s";
	private static final String endingEndpooint = "https://2captcha.com/res.php?key=%s&action=%s&id=%s";
	//https://2captcha.com/in.php?key=&method=&googlekey=&pageurl=
	
	
	public RecaptchaSolver() {
	}
	
	public static String solveRecaptcha(String googleKey, String pageUrl) {
		String endpoint = String.format(startingEndpoint, apiKey, "userrecaptcha", googleKey, pageUrl);
		HttpPost get = new HttpPost(endpoint);
		
		ArrayList<Object> response = ProxyUtil.executeRequest(get, false); 
		String message = (String) response.get(1);
		
		String jobId = message.split(Pattern.quote("|"))[1];
		String solved = getResponse(jobId); 
		return solved;
	}
	
	private static String getResponse(String jobId) {
		String endpoint = String.format(endingEndpooint, apiKey, "get", jobId);
		boolean waiting = true;
		String solve = "";
		while(waiting) {
			ArrayList<Object> response = ProxyUtil.executeRequest(new HttpGet(endpoint), false); 
			String message = (String) response.get(1);
			if(message.startsWith("OK")) {
				solve = message.split(Pattern.quote("|"))[1];
				waiting = false;
				break;
			} else {
				if(!message.contains("CAPCHA_NOT_READY"))
					GeneralUtil.print(message);
				if(message.contains("ERROR_CAPTCHA_UNSOLVABLE")) {
					return "";
				}
				
			}
			sleep(10000);
		}
		return solve;
	}
	
	private static void sleep(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}
	

}
