package repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.payments.Payment;

public interface PaymentsRepo extends MongoRepository<Payment, String>{
	
	public List<Payment> findByEmail(String email);
	public List<Payment> findByStatus(String status); 
	public List<Payment> findByEmailAndStatus(String email, String status);
	
	public Payment findByEmailAndPaymentDateId(String email, int paymentDateId);
	public Payment findByEmailAndPaymentId(String email, String paymentId);
}
