package repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.payment.dates.PaymentDate;

public interface PaymentDateRepo extends MongoRepository<PaymentDate, String>{

	public PaymentDate findByPaymentId(int paymentId); 
	public List<PaymentDate> findByActive(boolean active);
	
}
