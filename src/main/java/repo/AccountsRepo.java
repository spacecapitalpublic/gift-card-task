package repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.accounts.Account;

public interface AccountsRepo extends MongoRepository<Account, String>{
	
	public Account findByEmail(String email);
	public Account findByVerifyCode(String verifyCode);
	
}
