package repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.giftcards.Giftcard;
import data.model.giftcards.SettledGiftcard;

public interface SettledGiftcardRepo extends MongoRepository<SettledGiftcard, String>{
	public SettledGiftcard findByNumber(String number); 
	public SettledGiftcard findByEmailAndNumber(String email, String number);
	public List<SettledGiftcard> findByEmail(String email);
}
