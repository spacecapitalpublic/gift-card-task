package repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import data.model.offers.Offer;

public interface OffersRepo extends MongoRepository<Offer, String>{

	public Offer findBySourceName(String sourceName);
	
}
