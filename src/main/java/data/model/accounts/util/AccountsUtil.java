package data.model.accounts.util;

import java.util.regex.Pattern;

import data.model.accounts.AccountPayements;
import data.model.accounts.frontend.PayementResponse;

public class AccountsUtil {
	
	public static PayementResponse convertToFrontEnd(AccountPayements pay) {
		PayementResponse response = new PayementResponse();
		response.setPaypalEnabled(pay.isPaypalEnabled());
		response.setEcheckEnabled(pay.isEcheckEnabled());
		response.setPayementMethod(pay.getPreferredPayementMethod());
		
		if(!pay.getAccountNumber().isEmpty() && !pay.getRoutingNumber().isEmpty()) {
			if(pay.getAccountNumber().length() > 4)
				response.setAccountNumber("****"+pay.getAccountNumber().substring(pay.getAccountNumber().length()-4, pay.getAccountNumber().length()));
			else
				response.setAccountNumber(pay.getAccountNumber());
			
			if(pay.getRoutingNumber().length() > 4)
				response.setRouting("****"+pay.getRoutingNumber().substring(pay.getRoutingNumber().length()-4, pay.getRoutingNumber().length()));
			else
				response.setRouting(pay.getRoutingNumber());
		} else {
			response.setAccountNumber("");
			response.setRouting("");
		}
		
		if(!pay.getPaypalEmail().isEmpty()) {
			String name = pay.getPaypalEmail().split(Pattern.quote("@"))[0];
			String host = pay.getPaypalEmail().split(Pattern.quote("@"))[1]; 
			if(name.length() > 5)
				response.setPaypalEmail(name.substring(0, 3)+"****@"+host);
			else
				response.setPaypalEmail(name.substring(0,1)+"****@"+host);
		} else {
			response.setPaypalEmail("");
		}
		
		return response;
	}

}
