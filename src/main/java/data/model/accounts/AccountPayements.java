package data.model.accounts;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="AccountPayments")
public class AccountPayements {
	
	@Id
	private String id;
	
	private String email;
	
	private String preferredPayementMethod;

	private boolean paypalEnabled;
	private String paypalEmail;
	
	private boolean echeckEnabled;
	private String routingNumber;
	private String accountNumber;
	
	public String getPreferredPayementMethod() {
		return preferredPayementMethod;
	}
	public void setPreferredPayementMethod(String preferredPayementMethod) {
		this.preferredPayementMethod = preferredPayementMethod;
	}
	public boolean isPaypalEnabled() {
		return paypalEnabled;
	}
	public void setPaypalEnabled(boolean paypalEnabled) {
		this.paypalEnabled = paypalEnabled;
	}
	public String getPaypalEmail() {
		return paypalEmail;
	}
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	public boolean isEcheckEnabled() {
		return echeckEnabled;
	}
	public void setEcheckEnabled(boolean echeckEnabled) {
		this.echeckEnabled = echeckEnabled;
	}
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
