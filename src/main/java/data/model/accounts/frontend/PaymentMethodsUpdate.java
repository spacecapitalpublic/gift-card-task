package data.model.accounts.frontend;

public class PaymentMethodsUpdate {
	
	private String preferredMethod;
	
	private boolean paypalEnabled;
	private String paypalEmail;
	
	private boolean echeckEnabled;
	private String routingNumber;
	private String accountNumber;
	
	public String getPreferredMethod() {
		return preferredMethod;
	}
	public void setPreferredMethod(String preferredMethod) {
		this.preferredMethod = preferredMethod;
	}
	public boolean isPaypalEnabled() {
		return paypalEnabled;
	}
	public void setPaypalEnabled(boolean paypalEnabled) {
		this.paypalEnabled = paypalEnabled;
	}
	public String getPaypalEmail() {
		return paypalEmail;
	}
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	public boolean isEcheckEnabled() {
		return echeckEnabled;
	}
	public void setEcheckEnabled(boolean echeckEnabled) {
		this.echeckEnabled = echeckEnabled;
	}
	public String getRoutingNumber() {
		return routingNumber;
	}
	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
