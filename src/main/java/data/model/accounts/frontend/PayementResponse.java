package data.model.accounts.frontend;

public class PayementResponse {
	
	private String payementMethod;
	
	private boolean paypalEnabled;
	private String paypalEmail;

	private boolean echeckEnabled;
	private String routing;
	private String accountNumber;
	
	public String getPayementMethod() {
		return payementMethod;
	}
	public void setPayementMethod(String payementMethod) {
		this.payementMethod = payementMethod;
	}
	public boolean isPaypalEnabled() {
		return paypalEnabled;
	}
	public void setPaypalEnabled(boolean paypalEnabled) {
		this.paypalEnabled = paypalEnabled;
	}
	public String getPaypalEmail() {
		return paypalEmail;
	}
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	public boolean isEcheckEnabled() {
		return echeckEnabled;
	}
	public void setEcheckEnabled(boolean echeckEnabled) {
		this.echeckEnabled = echeckEnabled;
	}
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
}
