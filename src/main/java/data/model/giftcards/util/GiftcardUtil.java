package data.model.giftcards.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import data.model.backend.GiftcardBackend;
import data.model.giftcards.Giftcard;
import util.general.VersionController;

public class GiftcardUtil {
	
	public static Giftcard createNewGiftcard(String email, String cardNumber, String cardPin, double proclaimedBalance) {
		Giftcard card = new Giftcard();
		card.setCurrentStatus(VersionController.GIFTCARD_STATUS_VERIFYING);
		card.setEmail(email);
		card.setFailedSpendingReason("");
		card.setFailedVerifyReason("");
		card.setNumber(cardNumber);
		card.setPin(cardPin);
		card.setUploadDate(LocalDate.now());
		card.setProclaimedBalance(proclaimedBalance);
		return card;
	}
	
	public static GiftcardBackend convertToBackend(Giftcard card) {
		GiftcardBackend c = new GiftcardBackend();
		c.setNumber(card.getNumber());
		c.setPin(card.getPin());
		c.setUploadDate(card.getUploadDate().toString());
		c.setVerifiedBalance(card.getVerifiedBalance());
		c.setDiscount(1-card.getPayoutRate());
		return c;
	}
	
	public static List<GiftcardBackend> convertToBackendList(List<Giftcard> cards) {
		List<GiftcardBackend> list = new ArrayList<>();
		for(Giftcard card : cards) {
			if(card == null)
				continue;
			list.add(convertToBackend(card));
		}
		return list;
	}
}
