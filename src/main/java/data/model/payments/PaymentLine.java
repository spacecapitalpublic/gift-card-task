package data.model.payments;

public class PaymentLine {
	
	private String cardNumber;
	private double payoutBalance;
	private double balance;
	private double payoutRate;
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public double getPayoutBalance() {
		return payoutBalance;
	}
	public void setPayoutBalance(double payoutBalance) {
		this.payoutBalance = payoutBalance;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public double getPayoutRate() {
		return payoutRate;
	}
	public void setPayoutRate(double payoutRate) {
		this.payoutRate = payoutRate;
	}
	
}
