package data.model.payments.rest;

public class PaymentSubmit {
	
	private String databaseId;
	private String email;
	private String paymentId;
	
	private String paypalTransId;
	private String achTransId;
	
	public String getDatabaseId() {
		return databaseId;
	}
	public void setDatabaseId(String databaseId) {
		this.databaseId = databaseId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getPaypalTransId() {
		return paypalTransId;
	}
	public void setPaypalTransId(String paypalTransId) {
		this.paypalTransId = paypalTransId;
	}
	public String getAchTransId() {
		return achTransId;
	}
	public void setAchTransId(String achTransId) {
		this.achTransId = achTransId;
	}
}
