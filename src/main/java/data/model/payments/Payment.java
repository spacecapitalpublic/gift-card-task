package data.model.payments;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Payments")
public class Payment {
	
	@Id
	private String id;
	
	private String status;
	private String paymentId;
	
	private String email;
	private String method;
	private String paypalEmail;
	private String routing;
	private String accountNumber;
	
	private boolean paid;
	private LocalDate paidDate;
	
	private int paymentDateId;
	private LocalDate paymentDate;
	
	private double totalPayout;
	
	private HashSet<String> cardNumbers;
	private HashMap<String, PaymentLine> lines;
	
	private String paypalTransId;
	private String achTransId;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getPaypalEmail() {
		return paypalEmail;
	}
	public void setPaypalEmail(String paypalEmail) {
		this.paypalEmail = paypalEmail;
	}
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	public LocalDate getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(LocalDate paidDate) {
		this.paidDate = paidDate;
	}
	public int getPaymentDateId() {
		return paymentDateId;
	}
	public void setPaymentDateId(int paymentDateId) {
		this.paymentDateId = paymentDateId;
	}
	public LocalDate getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
	public double getTotalPayout() {
		return totalPayout;
	}
	public void setTotalPayout(double totalPayout) {
		this.totalPayout = totalPayout;
	}
	public HashSet<String> getCardNumbers() {
		return cardNumbers;
	}
	public void setCardNumbers(HashSet<String> cardNumbers) {
		this.cardNumbers = cardNumbers;
	}
	public HashMap<String, PaymentLine> getLines() {
		return lines;
	}
	public void setLines(HashMap<String, PaymentLine> lines) {
		this.lines = lines;
	}
	public String getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPaypalTransId() {
		return paypalTransId;
	}
	public void setPaypalTransId(String paypalTransId) {
		this.paypalTransId = paypalTransId;
	}
	public String getAchTransId() {
		return achTransId;
	}
	public void setAchTransId(String achTransId) {
		this.achTransId = achTransId;
	}
}
