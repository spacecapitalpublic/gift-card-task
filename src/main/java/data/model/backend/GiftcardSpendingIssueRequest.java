package data.model.backend;

public class GiftcardSpendingIssueRequest {
	
	private String reason;
	private double totalSpent;
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public double getTotalSpent() {
		return totalSpent;
	}
	public void setTotalSpent(double totalSpent) {
		this.totalSpent = totalSpent;
	}
	
	

}
