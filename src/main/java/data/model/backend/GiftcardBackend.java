package data.model.backend;

public class GiftcardBackend {

	private String uploadDate; 
	
	private String number;
	private String pin;
	private double verifiedBalance;
	private double discount;
	
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public double getVerifiedBalance() {
		return verifiedBalance;
	}
	public void setVerifiedBalance(double verifiedBalance) {
		this.verifiedBalance = verifiedBalance;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(double discount) {
		this.discount = discount;
	}
}
