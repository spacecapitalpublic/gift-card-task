package data.model.backend;

import java.util.List;

public class GiftcardGetResponse {
	
	private List<GiftcardBackend> giftcards;

	public List<GiftcardBackend> getGiftcards() {
		return giftcards;
	}
	public void setGiftcards(List<GiftcardBackend> giftcards) {
		this.giftcards = giftcards;
	}
}
