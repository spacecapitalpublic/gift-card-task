package data.model.backend;

public class GiftcardSpendRequest {
	private double amount;
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
}
