package rest.service.error;

public class ServiceError extends Exception{
	
	private String responseError;
	
	public ServiceError(String error) {
		this.responseError = error;
	}
	
	public String getResponseError() {
		return responseError;
	}
}