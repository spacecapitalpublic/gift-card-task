package main;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import controller.VerifyController;
import repo.GiftcardRepo;
import shceduled.controller.CoreScheduler;

@SpringBootApplication(exclude = {org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class})
@EnableMongoRepositories(basePackageClasses = {GiftcardRepo.class})
@ComponentScan(
		basePackageClasses = {CoreScheduler.class, VerifyController.class})
@EnableScheduling
public class TaskService {
	
	public static void main(String[] args) {
		SpringApplication.run(TaskService.class, args);
	}
	
}
